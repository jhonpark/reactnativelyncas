# Teste para React Native Developer

[![Lyncas Logo](https://img-dev.feedback.house/TCo5z9DrSyX0EQoakV8sJkx1mSg=/fit-in/300x300/smart/https://s3.amazonaws.com/feedbackhouse-media-development/modules%2Fcore%2Fcompany%2F5c9e1b01c5f3d0003c5fa53b%2Flogo%2F5c9ec4f869d1cb003cb7996d)](https://www.lyncas.net)

### Instalação
- Clonar o projeto.
- Navegar dentro da pasta e executar yarn install.
- Abrir o projeto no android Studio.
- react-native run-ios ou react-native run-android.

### Sobre o Projeto

- Não tive nenhuma dificudades em construir essa aplicação.
- As facilidades em desenvolver a aplicação foi a conexão com o firebase muito simples e ágil.
- O tempo gasto em média de 4 horas.

