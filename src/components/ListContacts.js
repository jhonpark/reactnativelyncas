import React from 'react';
import { View, Text, FlatList, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const ListContacts = ({ data, pressDelete, pressUpdate }) => (
  <FlatList
    data={data}
    keyExtractor={it => it.uid}
    renderItem={({ item }) => (
      <View style={styles.container}>
        <TouchableOpacity onPress={() =>{ console.log(item), pressUpdate(item)}} style={{ width: '70%' }}>
          <View style={{ marginLeft: 30 }}>
            <Text style={styles.txtName}>{item.nome} {item.sobreNome}</Text>
            <Text>{item.email}</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => pressDelete(item.uid)} style={{ marginRight: 30 }}>
          <Icon name="ios-trash" size={30} color="#15D3FC" />
        </TouchableOpacity>
      </View>
    )}
  />
)

const styles = {
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    borderBottomColor: "#CED0CE",
    borderBottomWidth: 1,
    padding: 10,
    marginBottom: 2
  },
  txtName: {
    fontSize: 14,
    color: "#000",
    fontWeight: "300"
  }
}

export default ListContacts;