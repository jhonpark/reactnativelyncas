import React from 'react';

import { View, TouchableOpacity, Text } from 'react-native';

const ButtonAux = ({ title, press, corTxt, corBack }) => (
    <View>
    <TouchableOpacity
        onPress={press}
        style={[styles.button, { backgroundColor: corBack }]}>
        <Text style={[styles.title, { color: corTxt }]}>{title}</Text>
    </TouchableOpacity>
    </View>
);
const styles = {
    button: {
        height: 40,
        alignItems: 'center',
        borderRadius: 50,
        justifyContent: 'center',
        elevation: 4
    },
    title: {
        fontSize: 15,
        fontWeight: '800'
    }
}

export default ButtonAux;
