import styled from 'styled-components';
import LinearGradient from 'react-native-linear-gradient';

export default styled(LinearGradient).attrs({
  colors: ['#FFF', '#b0bec5'],
  start: { x: 0, y: 0 },
  end: { x: 1, y: 1 }
})`
  flex: 1;
`