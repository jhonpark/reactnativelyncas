import React from 'react';
import { View, Text, Platform, StatusBar } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const Header = ({ txtHeader, iconName, press }) => (
    <View style={styles.viewHeader}>
        <StatusBar backgroundColor='#15D3FC' barStyle='light-content' />
        <View style={styles.viewSub}>
            <View style={{ marginLeft: 40, justifyContent: "center" }}>
                <Text style={styles.txtHeader}>{txtHeader}</Text>
            </View>
            <View style={{ marginRight: 20, justifyContent: "center" }}>
                <Icon
                    onPress={press}
                    name={iconName} size={35}
                    color="#FFFF"
                />
            </View>
        </View>
    </View>
)

const styles = {
    txtHeader: {
        textAlign: "center",
        color: "#FFF",
        fontSize: 20,
        fontWeight: "400"
    },
    viewHeader: {
        backgroundColor: "#15D3FC",
        height: 50,
        elevation: Platform.OS === "ios" ? 0 : 4,
    },
    viewSub: {
        flexDirection: "row",
        justifyContent: 'space-between',
        alingItens: 'center'
    },
    
}
export default Header;
