import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyDu88CrIDAutPQzKO9jg_f8W1mCRVNEYIk",
    authDomain: "lyncas-5e0db.firebaseapp.com",
    databaseURL: "https://lyncas-5e0db.firebaseio.com",
    projectId: "lyncas-5e0db",
    storageBucket: "lyncas-5e0db.appspot.com",
    messagingSenderId: "114064371",
    appId: "1:114064371:web:5097e858239523e6"
};

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();