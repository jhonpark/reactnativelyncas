import React, { PureComponent } from 'react';
import { View, Text, NetInfo, Dimensions, StyleSheet, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

const { width } = Dimensions.get('window');

function MiniOfflineSign() {
    return (
        <View style={styles.offlineContainer}>
            <Icon name='signal-wifi-off' size={22} color='#FFF' />
            <View style={{ marginRight: 25}}>
                <Text style={styles.offlineText}>Conectividade de rede limitada ou indisponível.</Text>
            </View>
        </View>
    );
}

class OfflineNotice extends PureComponent {

    state = {
        isConnected: true
    };
    componentWillMount() {
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
    }

    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    }

    handleConnectivityChange = isConnected => {
        if (isConnected) {
            this.setState({ isConnected });
        } else {
            this.setState({ isConnected });
        }
    };

    render() {
        if (!this.state.isConnected) {
            return <MiniOfflineSign />;
        }
        return null;
    }
}

const styles = StyleSheet.create({
    offlineContainer: {
        backgroundColor: '#b52424',
        height: 30,
        justifyContent: 'space-around',
        alignItems: 'center',
        flexDirection: 'row',
        width,
        position: 'absolute',
        bottom: 0,
        elevation: Platform.OS === "ios" ? 0 : 5
    },
    offlineText: {
        color: '#fff',
        fontSize: 12
    }
});

export default OfflineNotice;
