import React, { Component } from 'react';

import './config/firebase';
import Routes from './Routes';
import OfflineNotice from './config/OfflineNotice';


class App extends Component {
    render() {
        return (
            <Routes />
        );
    }
}

export default App;
