import React, { Component } from 'react';
import { Text, TextInput, View, ScrollView, Alert, ActivityIndicator, StatusBar, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import VMasker from 'vanilla-masker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import axios from 'axios';
import b64 from 'base-64';
import _ from 'lodash';
import firebase from 'firebase';

import { Container, ButtonAux, Header } from '../../components';
import Offline from '../../config/OfflineNotice';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 30 : StatusBar.currentHeight;

export default class Contacts extends Component {

  state = {
    nome: "",
    sobreNome: "",
    telefone: "",
    cep: "",
    rua: "",
    bairro: "",
    cidade: "",
    uf: "",
    numero: "",
    email: "",
    uid: "",
    loading: false
  }

  componentDidMount() {
    const { data } = this.props.navigation.state.params;
    if (data) {
      this.setState({
        nome: data.nome,
        sobreNome: data.sobreNome,
        telefone: data.telefone,
        cep: data.cep,
        rua: data.rua,
        bairro: data.bairro,
        cidade: data.cidade,
        uf: data.uf,
        numero: data.numero,
        email: data.email,
        uid: data.uid
      })
    }
  }

  getCep = () => {
    const { cep } = this.state;
    axios.get(`https://viacep.com.br/ws/${cep}/json/`, {
      timeout: 10000,
      headers: { 'Content-Type': 'application/json' }
    }).then(response => {
      if (response.status === 200) {
        this.setState({
          rua: response.data.logradouro,
          bairro: response.data.bairro,
          cidade: response.data.localidade,
          uf: response.data.uf,
        })
      }
    }).catch(error => {

    });
  }

  handleContact = () => {
    const { nome, sobreNome, telefone, cep, rua, bairro, cidade, uf, numero, email } = this.state;
    //email do usuário autenticado
    const { currentUser } = firebase.auth();
    let emailUsuarioB64 = b64.encode(currentUser.email);
    if (nome != '' && cep != '') {
      this.setState({ loading: true });
      firebase.database().ref(`/usuario_contatos/${emailUsuarioB64}`)
        .push({ nome, sobreNome, telefone, email, cep, rua, bairro, cidade, uf, numero })
        .then(response => {
          this.setState({ loading: false });
          this.props.navigation.navigate('Main');
        })
        .catch(erro => {
          Alert.alert("Erro!", "Erro ao salvar dados!")
          this.setState({ loading: false });
        })
    } else {
      Alert.alert("Atenção", "verifique se todos os campos estão preenchidos corretamente.")
    }
  }

  handleUpdateContatct = () => {
    const { nome, sobreNome, telefone, cep, rua, bairro, cidade, uf, numero, email, uid } = this.state;
    const { currentUser } = firebase.auth();

    let emailUsuarioB64 = b64.encode(currentUser.email);
    const updateContactRef = firebase.database().ref(`/usuario_contatos/${emailUsuarioB64}/${uid}`);

    updateContactRef.update({
      nome: nome,
      sobreNome: sobreNome,
      telefone: telefone,
      cep: cep,
      rua: rua,
      bairro: bairro,
      cidade: cidade,
      uf: uf,
      numero: numero,
      email: email,
    });
    this.props.navigation.navigate('Main');
  };

  render() {
    const { nome, sobreNome, telefone, cep, rua, bairro, cidade, uf, numero, email, loading } = this.state;
    const { update } = this.props.navigation.state.params;
    return (
      <Container>
        <View style={[styles.statusBar, styles.statusIos]}>
          <StatusBar barStyle="light-content" backgroundColor="#15D3FC" />
        </View>
        <Header
          iconName="ios-undo"
          txtHeader={!update ? "Adicionar Contato" : "Atualizar Contato"}
          press={() => this.props.navigation.navigate('Main')}
        />
        <ScrollView>
          <View>
            <KeyboardAwareScrollView enableOnAndroid>
              <View style={{ marginTop: 20, marginLeft: 30, marginBottom: 10 }} >
                <Text>Pessoal</Text>
              </View>
              <View style={styles.container}>
                <Icon style={{ paddingRight: 5 }} name="ios-person" size={24} color="#15D3FC" />
                <TextInput
                  style={styles.txtInput}
                  value={nome}
                  editable
                  autoCorrect={false}
                  onChangeText={(value) => this.setState({ nome: value })}
                  multiline={false}
                  underlineColorAndroid='transparent'
                  placeholder='Nome'
                />
              </View>
              <View style={[styles.container, { paddingLeft: 30 }]}>
                <TextInput
                  style={styles.txtInput}
                  value={sobreNome}
                  editable
                  autoCorrect={false}
                  onChangeText={(value) => this.setState({ sobreNome: value })}
                  multiline={false}
                  underlineColorAndroid='transparent'
                  placeholder='Sobrenome'
                />
              </View>
              <View style={styles.container}>
                <Icon style={{ paddingRight: 5 }} name="ios-call" size={24} color="#15D3FC" />
                <TextInput
                  style={styles.txtInput}
                  value={telefone}
                  editable
                  autoCorrect={false}
                  onChangeText={(value) => this.setState({ telefone: VMasker.toPattern(value, '(99) 99999-9999') })}
                  multiline={false}
                  underlineColorAndroid='transparent'
                  placeholder='Telefone'
                  keyboardType="numeric"
                />
              </View>
              <View style={styles.container}>
                <Icon style={{ paddingRight: 5 }} name="ios-mail" size={24} color="#15D3FC" />
                <TextInput
                  style={styles.txtInput}
                  value={email}
                  editable
                  autoCorrect={false}
                  onChangeText={(value) => this.setState({ email: value })}
                  multiline={false}
                  underlineColorAndroid='transparent'
                  placeholder='E-mail'
                  autoCapitalize="none"
                  keyboardType="email-address"
                />
              </View>
              <View style={{ paddingTop: 10, marginLeft: 30 }} >
                <Text>Endereço</Text>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={[styles.container, { width: '45%' }]}>
                  <Icon style={{ paddingRight: 5 }} name="ios-pin" size={24} color="#15D3FC" />
                  <TextInput
                    style={styles.txtInput}
                    value={cep}
                    editable
                    autoCorrect={false}
                    onChangeText={(value) => this.setState({ cep: VMasker.toPattern(value, '99999-999') })}
                    multiline={false}
                    underlineColorAndroid='transparent'
                    placeholder='Cep'
                    keyboardType="numeric"
                    onBlur={this.getCep}
                  />
                </View>
                <View style={[styles.container, { width: '54%' }]}>
                  <TextInput
                    style={styles.txtInput}
                    value={cidade}
                    editable
                    autoCorrect={false}
                    onChangeText={(value) => this.setState({ cidade: value })}
                    multiline={false}
                    underlineColorAndroid='transparent'
                    placeholder='Cidade'
                  />
                </View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={[styles.container, { paddingLeft: 30, width: "70%" }]}>
                  <TextInput
                    style={styles.txtInput}
                    value={rua}
                    editable
                    autoCorrect={false}
                    onChangeText={(value) => this.setState({ rua: value })}
                    multiline={false}
                    underlineColorAndroid='transparent'
                    placeholder='Rua'
                  />
                </View>
                <View style={[styles.container, { width: '29%' }]}>
                  <TextInput
                    style={styles.txtInput}
                    value={uf}
                    editable
                    autoCorrect={false}
                    onChangeText={(value) => this.setState({ uf: value })}
                    multiline={false}
                    underlineColorAndroid='transparent'
                    placeholder='UF'
                  />
                </View>
              </View>
              <View style={{ flexDirection: "row" }}>
                <View style={[styles.container, { width: '50%', paddingLeft: 30 }]}>
                  <TextInput
                    style={styles.txtInput}
                    value={numero}
                    editable
                    autoCorrect={false}
                    onChangeText={(value) => this.setState({ numero: value })}
                    multiline={false}
                    underlineColorAndroid='transparent'
                    placeholder='Número'
                    keyboardType="numeric"
                  />
                </View>
                <View style={[styles.container, { width: '49%' }]}>
                  <TextInput
                    style={styles.txtInput}
                    value={bairro}
                    editable
                    autoCorrect={false}
                    onChangeText={(value) => this.setState({ bairro: value })}
                    multiline={false}
                    underlineColorAndroid='transparent'
                    placeholder='Bairro'
                  />
                </View>
              </View>
            </KeyboardAwareScrollView>
          </View>
        </ScrollView>
        {!update && <View style={styles.btnArea}>
          {loading ? <ActivityIndicator size="large" color="#15D3FC" /> : <ButtonAux
            corBack='#15D3FC'
            corTxt='#FFF'
            title="ADICIONAR"
            press={() => this.handleContact()}
          />}
        </View>}
        {update && <View style={styles.btnArea}>
          {loading ? <ActivityIndicator size="large" color="#15D3FC" /> : <ButtonAux
            corBack='#15D3FC'
            corTxt='#FFF'
            title="ATUALIZAR"
            press={() => this.handleUpdateContatct()}
          />}
        </View>}   
        <Offline />    
      </Container>
    )
  }
}

const styles = {
  container: {
    flexDirection: "row",
    paddingLeft: 10,
    paddingRight: 10,
    alignItems: 'center',
  },
  txtInput: {
    flex: 1,
    borderBottomColor: "black",
    borderBottomWidth: 1
  },
  btnArea: {
    paddingLeft: '15%',
    paddingRight: '15%',
    marginBottom: 25
  },
  backButton: {
    width: 40,
    height: 40,
    alignItems: 'center',
  },
  txtHeader: {
    textAlign: "center",
    color: "#000",
    fontSize: 16,
    marginLeft: 20
  },
  statusIos: {
    backgroundColor: "#15D3FC",  
    elevation: Platform.OS === "ios" ? 0 : 4
  },
  statusBar: {
    height: STATUSBAR_HEIGHT
  },
}
