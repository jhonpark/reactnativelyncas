import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  TouchableOpacity,
  Alert,
  ActivityIndicator,
  StatusBar
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import firebase from 'firebase';
import { StackActions, NavigationActions } from 'react-navigation';

import { ButtonAux, Container } from '../../components/';
import OfflineNotice from '../../config/OfflineNotice';

const logo = require('../../assets/icon.png');

class Login extends Component {
  state = {
    email: '',
    password: '',
    loading: false,
    isLogged: false,
  }

  isLogged = () => {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.resetRoutes();
      }
    })
  }

  componentDidMount() {
    this.isLogged();
  }

  resetRoutes = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Main' })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  autenticarUsuario = async () => {
    const { email, password, loading } = this.state;
    if (email != '' && password != '') {
      this.setState({ loading: true });
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then(response => {
          this.setState({
            loading: false,
            email: '',
            password: ''
          });
          this.resetRoutes();
        })
        .catch(erro => {
          let messageErro = '';
          this.setState({ loading: false });
          console.log(erro)
          switch (erro.code) {
            case 'auth/invalid-email':
              messageErro = 'O endereço de email está mal formatado';
              break;
            case 'auth/wrong-password':
              messageErro = 'A senha é inválida ou o usuário não possui uma senha.';
              break;
            case 'auth/user-not-found':
              messageErro = 'Não há registro de usuário correspondente. O usuário pode ter sido excluído.'
            case 'auth/network-request-failed':
              messageErro = 'Ocorreu um erro de rede (como tempo limite, conexão interrompida ou host inacessível).'
              break;
            default:
              break;
          }
          Alert.alert("Erro!", messageErro);

        })
    } else {
      Alert.alert("Erro!", "E-mail ou Senha não preenchidos!");
    }
  }

  render() {
    const { email, password, loading } = this.state;
    return (
      <Container>
        <StatusBar backgroundColor='#FFF' barStyle='dark-content' />
        <KeyboardAwareScrollView enableOnAndroid>
          <View style={{ flex: 1, paddingTop: 90, alignItems: 'center' }}>
            <Image style={styles.imgBottom} resizeMode='contain' source={logo} />
            <View style={{ width: 280 }}>
              <Text style={styles.txtPlaceHolder}>E-MAIL</Text>
              <TextInput
                value={email}
                style={styles.txtInput}
                onChangeText={value => this.setState({ email: value })}
                autoCapitalize='none'
                keyboardType='email-address'
              />
            </View>
            <View style={{ width: 280, paddingTop: 20 }}>
              <Text style={styles.txtPlaceHolder}>SENHA</Text>
              <TextInput
                value={password}
                style={styles.txtInput}
                onChangeText={value => this.setState({ password: value })}
                autoCapitalize='none'
                secureTextEntry
                onSubmitEditing={() => this.autenticarUsuario()}
              />
            </View>
            <View style={{ width: 150, paddingTop: 40 }} >
              {loading ? <ActivityIndicator size='large' color='#15D3FC' />
                :
                <ButtonAux
                  corBack='#15D3FC'
                  corTxt='#FFF'
                  title="LOGIN"
                  press={() => this.autenticarUsuario()}
                />}
            </View>
            <TouchableOpacity style={{ paddingTop: 40 }} onPress={() => this.props.navigation.navigate('Register')}>
              <Text>Cadastrar Usuário</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
        <OfflineNotice />
      </Container>
    );
  }
}

const styles = {
  welcome: {
    fontSize: 15,
    textAlign: 'center',
    margin: 20,
    color: '#485bff'
  },
  txtInput: {
    flex: 1,
    borderBottomColor: "black",
    borderBottomWidth: 1,
  },
  buttoEnter: {
    backgroundColor: '#039BE5',
    height: 40,
    alignItems: 'center',
    borderRadius: 25,
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#37474F'
  },
  imgBottom: {
    height: 70,
    width: 300,
    margin: 20
  },
  txtPlaceHolder: {
    color: '#37474F',
    fontWeight: '800',
    marginBottom: 8,
    marginLeft: 5
  }
};

export default Login;

