import React, { Component } from 'react';
import firebase from 'firebase';
import VMasker from 'vanilla-masker';
import { View, Text, TextInput, Alert, ActivityIndicator, TouchableOpacity } from 'react-native';
import b64 from 'base-64';
import Icon from 'react-native-vector-icons/Ionicons';
import { StackActions, NavigationActions } from 'react-navigation';

import { Container, ButtonAux } from '../../components/';
import Offline from '../../config/OfflineNotice';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

class Register extends Component {

  state = {
    nome: '',
    sobreNome: '',
    telefone: '',
    email: '',
    password: '',
    loading: false
  }

  resetRoutes = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Main' })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  handleUser = () => {
    const { nome, sobreNome, telefone, email, password, loading } = this.state;
    if (nome != '' && sobreNome != '' && telefone != '' && email != '' && password != '') {
      this.setState({ loading: true });
      firebase.auth().createUserWithEmailAndPassword(email, password)
        .then(user => {
          let emailB64 = b64.encode(email);
          firebase.database().ref(`/contatos/${emailB64}`)
            .push({ nome, sobreNome, telefone })
            .then(res => {
              this.setState({ loading: false });
              this.resetRoutes();
            })
        })
        .catch(erro => {
          this.setState({ loading: false });
          switch (erro.code) {
            case "auth/email-already-in-use":
              alert("O endereço de e-mail já está sendo usado por outra conta.")
              break;
            case "auth/weak-password":
              alert("a senha deve ter pelo menos 6 caracteres.")
              break;
            case 'auth/network-request-failed':
              alert('Ocorreu um erro de rede (como tempo limite, conexão interrompida ou host inacessível).')
            default:
              break;
          }
        });
    } else {
      Alert.alert("Campos", "Por favor, verifique se todos os campos estão preenchidos corretamente.");
    }
  }

  render() {
    const { nome, sobreNome, telefone, email, password, loading } = this.state;
    return (
      <Container>
        <Offline />
        <TouchableOpacity style={styles.backButton} onPress={() => this.props.navigation.navigate('Login')}>
          <Icon name="ios-arrow-round-back" size={40} color="#15D3FC" />
        </TouchableOpacity>
        <Text style={styles.txtHeader}>
          Cadastro
            </Text>
        <KeyboardAwareScrollView enableOnAndroid >
          <View style={styles.form}>
            <View style={styles.boxInput}>
              <TextInput
                value={nome}
                editable
                autoCorrect={false}
                onChangeText={(value) => this.setState({ nome: value })}
                multiline={false}
                underlineColorAndroid='transparent'
                placeholder='Nome'
                style={styles.txtInput}
              />
            </View>
            <View style={styles.boxInput}>
              <TextInput
                value={sobreNome}
                editable
                autoCorrect={false}
                onChangeText={(value) => this.setState({ sobreNome: value })}
                multiline={false}
                underlineColorAndroid='transparent'
                placeholder='Sobrenome'
                style={styles.txtInput}
              />
            </View>
            <View style={styles.boxInput}>
              <TextInput
                value={telefone}
                editable
                autoCorrect={false}
                onChangeText={(value) => this.setState({ telefone: VMasker.toPattern(value, '(99) 99999-9999') })}
                multiline={false}
                underlineColorAndroid='transparent'
                placeholder='Telefone'
                style={styles.txtInput}
                keyboardType='numeric'
              />
            </View>
            <View style={styles.boxInput}>
              <TextInput
                value={email}
                editable
                autoCorrect={false}
                onChangeText={(value) => this.setState({ email: value })}
                multiline={false}
                underlineColorAndroid='transparent'
                placeholder='E-mail'
                keyboardType='email-address'
                style={styles.txtInput}
                autoCapitalize="none"
              />
            </View>
            <View style={styles.boxInput}>
              <TextInput
                value={password}
                editable
                autoCorrect={false}
                onChangeText={(value) => this.setState({ password: value })}
                multiline={false}
                underlineColorAndroid='transparent'
                style={styles.txtInput}
                placeholder='Senha'
                secureTextEntry={true}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>
        <View style={styles.btnArea}>
          {loading ? <ActivityIndicator size='large' color='#15D3FC' />
            :
            <ButtonAux
              corBack='#15D3FC'
              corTxt='#FFF'
              title="CADASTRAR"
              press={() => this.handleUser()}
            />}
        </View>
      </Container>
    )
  }
}

const styles = {
  btnArea: {
    paddingLeft: '15%',
    paddingRight: '15%',
    marginBottom: 20
  },
  form: {
    paddingTop: 40,
    alignItems: 'center',
    paddingLeft: 16,
    paddingRight: 16
  },
  txtHeader: {
    fontSize: 20,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  boxInput: {
    width: 280,
    height: 40
  },
  backButton: {
    width: 40,
    height: 40,
    alignItems: 'center',
  },
  txtInput: {
    flex: 1,
    borderBottomColor: "black",
    borderBottomWidth: 1
  },
}

export default Register;
