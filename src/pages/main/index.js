import React, { Component } from 'react';
import {
  getBottomSpace,
} from "react-native-iphone-x-helper";
import { StackActions, NavigationActions } from 'react-navigation';
import { View, StyleSheet, TouchableOpacity, Alert, StatusBar, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import firebase from 'firebase';
import b64 from 'base-64';

import { ListContacts, Header, Container } from '../../components';
import OfflineNotice from '../../config/OfflineNotice';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 30 : StatusBar.currentHeight;

export default class Main extends Component {
  state = {
    contacts: []
  }

  contatosUsuarioFetch = () => {
    const { currentUser } = firebase.auth();
    let emailUsuarioB64 = b64.encode(currentUser.email);
    firebase.database().ref(`/usuario_contatos/${emailUsuarioB64}`)
      .on("value", snapshot => {
        const response = snapshot.val();
        console.log(response)
        const data = !!response ? Object.keys(response).map(uid => ({
          ...response[uid],
          uid
        })) : [];
        this.setState({ contacts: data });
      })
  }

  componentDidMount() {
    this.contatosUsuarioFetch();
  }

  handleDelete = uid => {
    const { currentUser } = firebase.auth();
    Alert.alert('Exclusão',
      'Deseja excluir esse contato?',
      [{
        text: 'OK', onPress: () => {
          let emailUsuarioB64 = b64.encode(currentUser.email);
          const contactsRef = firebase.database().ref(`/usuario_contatos/${emailUsuarioB64}/${uid}`);
          contactsRef.remove();
        }
      },
      { text: 'Cancelar', onPress: () => { } }]
    );
  };

  resetRoutes = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: 'Login' })],
    });
    this.props.navigation.dispatch(resetAction);
  }

  logout = () => {
    firebase.auth().signOut()
      .then(logout => {
        this.resetRoutes();
      })
  }

  render() {
    return (
      <Container>
        <View style={[styles.statusBar, styles.statusIos]}>
          <StatusBar barStyle="light-content" backgroundColor="#15D3FC" />
        </View>
        <Header txtHeader="Contatos" iconName="ios-log-out" press={() => { this.logout() }} />
        <ListContacts
          style={{ marginTop: 10 }}
          data={this.state.contacts}
          pressDelete={(item) => this.handleDelete(item)}
          pressUpdate={(data) => this.props.navigation.navigate('Contacts', { data, update: true })}
        />
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate('Contacts', { update: false })}
          style={styles.fab}>
          <Icon name='ios-add' size={40} color='#FFF' />
        </TouchableOpacity>
        <OfflineNotice />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  fab: {
    position: "absolute",
    right: 30,
    bottom: 30 + getBottomSpace(),
    width: 60,
    height: 60,
    backgroundColor: "#15D3FC",
    borderRadius: 30,
    alignItems: "center",
    justifyContent: "center",
    elevation: 4
  },
  statusBar: {
    height: STATUSBAR_HEIGHT
  },
  statusIos: {
    backgroundColor: "#15D3FC",
    elevation: Platform.OS === "ios" ? 0 : 4
  }

})
