import { createAppContainer, createStackNavigator } from 'react-navigation';

import Login from './pages/login';
import Main from './pages/main';
import Register from './pages/register';
import Contacts from './pages/contacts';

const Routes = createAppContainer(
    createStackNavigator({
        Login,
        Main,
        Register,
        Contacts
    },
        {
            headerMode: 'none'
        }
    )
);

export default Routes;